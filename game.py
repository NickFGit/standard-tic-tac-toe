def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board,num):
    print_board(board)
    print("GAME OVER")
    print(board[num], "has won")
    return exit()

def is_row_winner(board,num):
    space = int((num * (1/3)) + (2/3))
    return bool(board[space - 1] == board[space] and board[space] == board[space + 1])

def is_col_winner(board, num):
    return bool(board[num - 1] == board[num + 2] and board[num + 2] == board[num + 5])

def is_diag_winner(board, num):
    return bool(board[1 + num] == board[4] and board[4] == board[7 + num])

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board,1) or is_col_winner(board,1) or is_diag_winner(board,1):
        game_over(board,0)
    elif is_col_winner(board,2):
        game_over(board,1)
    elif is_col_winner(board,3) or is_diag_winner(board,-1):
        game_over(board,2)
    elif is_row_winner(board,2):
        game_over(board,3)
    elif is_row_winner(board,3):
        game_over(board,6)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
